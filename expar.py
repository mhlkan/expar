from bs4 import BeautifulSoup as bs
from time import sleep as wait
import requests as rq
import json
import yaml
def main():
    star_list = []
    i = 1
    link = 'https://github.com/' + str(input('https://github.com/')) + '?tab=stars'
    while True:
        r = rq.get(link + '&page=' + str(i))
        html = r.content
        soup = bs(html, 'html.parser')
        stars = soup.find_all('div', {'class': 'd-inline-block mb-1'})
        blank_slate = soup.find_all('div', {'class': 'blankslate mt-5'})
        if blank_slate:
            break
        for star in stars:
            star_data = {
                'name': star.find('a')['href'],
                'link': star.find('a')['href']
            }
            print('\rAdding',star_data['name'])
            star_list.append(star_data)
            wait(0.1)
        i += 1
    star_list = list(star_list)
    save_name = str(input('Save as '))
    while 1:
        if save_name.endswith('.json'):
            with open(save_name, 'w') as f:
                f.write(json.dumps(star_list))
            print('Complete.')
            break
        elif save_name.endswith('.yml'):
            with open(save_name, 'w') as f:
                yaml.dump(
                    star_list,
                    f,
                    explicit_start=True,
                    default_flow_style=False
                )
            print('Complete.')
            break
        else:
            print('Unknown file type.')
if __name__ == '__main__':
    main()